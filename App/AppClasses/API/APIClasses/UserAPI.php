<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 09.06.14
 * Time: 15:41
 */

namespace App\AppClasses\API\APIClasses;

use App\Services\Classes\APIKeyChecker;
use App\Services\Classes\ServiceManager;
use App\AppClasses\Models\User;

class UserAPI extends AbstractAPI
{
    protected $User;

    public function __construct($request)
    {
        parent::__construct($request);

        /** @var APIKeyChecker $apiChecker */
        $apiChecker = ServiceManager::get('api_checker');
        /** @var User $User */
        $User = new User();
        /** @var ServiceManager $sv */

        if (!array_key_exists('api_key', $this->request)) {
            throw new \Exception('No API Key provided');
        } else if (!$apiChecker->verifyKeyAndHost(array('api_key' => $this->request['api_key']))) {
            throw new \Exception('Invalid API Key or your host is not in list of allowed hosts');
        } else if (!$apiChecker->verifyHost($_SERVER['REMOTE_ADDR'])) {
            throw new \Exception('your host is not in the list of allowed hosts');
        }

        $this->User = $User;
    }

    /** check username & password  */
    protected function login()
    {
        if (empty($_REQUEST['username']) or empty($_REQUEST['password'])) {
            throw new \Exception('no password or no username, check request parameters');
        }
        $username = $_REQUEST['username'];
        $password = mb_strtoupper($_REQUEST['password']);
        $connectionParams = ServiceManager::get('config_manager')->get('billing_oracle_devel_base');
        if (empty($connectionParams['host']) or empty($connectionParams['user']) or empty($connectionParams['password'])
            or empty($connectionParams['sid'])
        ) {
            throw new \Exception('not enough params to connect to oracle ,check config.ini');
        }
        $host = $connectionParams['host'];
        $user = $connectionParams['user'];
        $connectPassword = $connectionParams['password'];
        $sid = $connectionParams['sid'];
        if(!$connection = ServiceManager::get('oracle_db_manager')->getConnection($host, $user, $connectPassword, $sid)) {
            throw new \Exception(" can't connect to oracle db");
        };

        $procedure = "eas_user_login('$username','$password', :refc)";
        /** @var \ADORecordSet $recordSet */
        $queryResult = ServiceManager::get('oracle_db_manager')->executeCursor($procedure, $connection);
//        var_dump($queryResult[0]);

         if(!empty($queryResult[0]) && isset($queryResult[0]['ID']) && $queryResult[0]['CODE'] === $username) {
            return $queryResult[0];
        } else {
            throw new \Exception('login failed check your username\password');
        }
    }

    /**
     * Example of an Endpoint
     */
    protected function example()
    {
        if ($this->method == 'GET') {
            return "Your name is " . $this->User->name;
        } else {
            return "Only accepts GET requests";
        }
    }
} 
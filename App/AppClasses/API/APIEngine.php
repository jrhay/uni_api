<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 19.06.14
 * Time: 14:54
 */

namespace App\AppClasses\API;

use App\AppClasses\API\APIClasses;
use App\Services\Classes\ServiceManager;

/** class APIEngine choose api using url end call it , also APIEngine print output result in json*/
class APIEngine
{
    protected $_subREQUEST;
    protected $apiName;

    //Статичная функция для подключения API из других API при необходимости в методах
    protected function getApiObjectByName($apiName)
    {

        $apiClass = ServiceManager::get('config_manager')->get('api_classes/' . $apiName);
        $apiClass = 'App\AppClasses\API\APIClasses\\' . $apiClass;
        try {
            $subRequestString = $this->_subREQUEST['request'];
            $apiObject = new  $apiClass($subRequestString);
            return $apiObject;
        } catch
        (\Exception $e) {
            echo json_encode(Array('error' => $e->getMessage()));
            exit();
        }
    }

    /** __construct gets request */
    function __construct($_request)
    {
        $this->getSubRequest($_request);
    }

    /** getSubReauest gets request as a param and explode it in apiName and subRequest */
    protected function getSubRequest($_request)
    {
        $requestString = $_request['request'];
        $requestParts = explode('/', rtrim($requestString, '/'));
        $this->apiName = array_shift($requestParts);
        if (!$this->apiName) $this->apiName = 'help';
        $subRequest = implode('/', $requestParts);
        $_request['request'] = $subRequest;
        $this->_subREQUEST = $_request;
    }

    public function callApiFunction()
    {
        /** @var AppClasses\AbstractAPI $apiObject */
        $apiObject = $this->getApiObjectByName($this->apiName);
        try {
            return $apiObject->processAPI();
        } catch
        (\Exception $e) {
            return json_encode(Array('error' => $e->getMessage()));
        }
    }
}

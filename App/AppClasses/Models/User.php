<?php

namespace App\AppClasses\Models;


class User
{
    // ID	NUMBER(12,0) - key
    protected $id;
    // GUID	VARCHAR2(36 BYTE) - something strange ( no explanation in database )
    protected $guid;
    // CODE	VARCHAR2(30 BYTE) -login
    protected $code;
    // LAST_NAME    VARCHAR2(64 BYTE) - surname
    protected $lasteName;
    // FIRST_NAME    VARCHAR2(64 BYTE) - name
    protected $firstName;
    // MIDDLE_NAME    VARCHAR2(64 BYTE) - middle name
    protected $middleName;
    // TITLE    VARCHAR2(255 BYTE) - position ( interesting, why operating position called title in database)
    protected $title;
    // PHONE    VARCHAR2(255 BYTE) - phone
    protected $phone;
    // EMAIL    VARCHAR2(255 BYTE) - email
    protected $email;
    // STATUS    CHAR(4 BYTE) - status
    protected $status;
    // PASSWORD    RAW - password ( hash md5)
    protected $password;
    // EMAIL2    VARCHAR2(255 BYTE) - alternative mail
    protected $mail2;
    // MOBILE_PHONE    VARCHAR2(255 BYTE)
    protected $mobilePhone;
    // CREAT_DATE    DATE - user create date
    protected $createDate;
    // CREAT_USER    VARCHAR2(30 BYTE) - ( no explanation in database )
    protected $createUser;
    // AUDIT_DATE    DATE - ( no explanation in database )
    protected $auditDate;
    //AUDIT_USER    VARCHAR2(30 BYTE) - ( no explanation in database )
    protected $auditUser;
    //TMSTAMP    TIMESTAMP(6)  - ( no explanation in database )
    protected $tmstamp;
    //NAME    VARCHAR2(255 BYTE) - ( no explanation in database )
    public $name;

    /** set some field in User
     * input params : array($fieldName1 => $value1, $fieldName2 => $value2,...) , where $fieldName1... is the name of the field and $value1... is the value
     * return true if ok and throw exception in otherway
     * @param  array $array
     * @return mixed$
     */
    public function  set($setPairs = array())
    {
        if (empty($setPairs)) {
            throw new \Exception("Trying to handle empty input array in set()");
        }
        foreach ($setPairs as $fieldName => $value) {
            if (!property_exists($this, $fieldName)) {
                throw new \Exception("Model " . __CLASS__ . "does not contain property" . $fieldName);
            }
            if ($fieldName === 'id' or $fieldName === "guid") {
                throw new \Exception("Can't change " . $fieldName . " in " . __CLASS__ . " model ");
            }
            $this->$fieldName = $value;
            return true;
        }
        return false;
    }

    public function get($fieldName, $stringToCompare)
    {
        if (!is_string($fieldName)) {
            throw new \Exception("Input parameter " . $fieldName . " is not string");
        }
        if (!property_exists($this, $fieldName)) {
            throw new \Exception("Can't get or compare " . $fieldName . " no such field in " . __CLASS__);
        }
        if (!$stringToCompare) {
            return $this->{$fieldName};
        } else {
            return $this->{$fieldName} == $stringToCompare;
        }
    }
} 
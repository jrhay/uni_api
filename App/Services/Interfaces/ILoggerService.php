<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 10.06.14
 * Time: 15:49
 */

namespace App\Services\Interfaces;

/** Logger interface */
interface ILoggerService
{
    /** log function takes message and logLevel as argument and logging it to file in /logs, you can use one of
     * standart log levels or yous our own.
     * @param string $message
     * @param string $log_level
     * @return bool
     */
    public function log($message, $logLevel);
}
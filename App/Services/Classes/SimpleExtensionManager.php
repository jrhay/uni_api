<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 24.06.14
 * Time: 17:44
 */

namespace App\Services\Classes;


use App\Services\Interfaces\IExtensionManagerService;

class SimpleExtensionManager extends AbstractSingletonService implements IExtensionManagerService {

    /** @param string $extName - like 'oracleDB' or 'mysqlDB' assigned in config.yml */
    public function getExtensionObject($extensionName)
    {
        $fullExtClassName = $this->getExtFilePath($extensionName);
        if(class_exists($fullExtClassName)) {
            $ext = new $fullExtClassName();
        } else {
            throw new \Exception("can't find class $fullExtClassName ");
        }
        return $ext;
    }

    /** create object of extension class or require ext script ( is no class in assigned file)*/
    public function  getExtension($extensionName)
    {
        $fullExtFileName = $this->getExtFilePath($extensionName);
        if(class_exists($fullExtFileName)) {
            $this->getExtensionObject($extensionName);
        } else if (file_exists( PROJECT_ROOT_DIR. DIRECTORY_SEPARATOR . $fullExtFileName . '.php')) {
            $this->requireExtensionFile($extensionName);
        } else {
            throw new \Exception("can\'t get extension $fullExtFileName, no such class, no such file");
        }
    }

    /** require extension .php file
     * @param $extension
     */
    public function requireExtensionFile($extensionName)
    {
        $extFilePath = $this->getExtFilePath($extensionName);
        require_once(PROJECT_ROOT_DIR. DIRECTORY_SEPARATOR. $extFilePath . '.php');
    }

    /** returns extension file path */
    protected function getExtFilePath($extensionName){
        $extensionFilename = ServiceManager::get('config_manager')->get('extensions/'. $extensionName);
        $extDir = 'App' . DIRECTORY_SEPARATOR . 'Extensions' ;
        $fullExtFilename = $extDir .  DIRECTORY_SEPARATOR . $extensionFilename ;
        $fullExtFilename = $this->refactorExtensionName($fullExtFilename);
        return $fullExtFilename;
    }

    /** refactor extension name before use it ( change directory separator) with this function
     * @param string $extensionName
     * @return string $refactoredExtensionName*/
    protected function refactorExtensionName($extensionName){
        $directorySeparators = array('\\','/');
        $refactoredExtensionName = str_replace($directorySeparators,DIRECTORY_SEPARATOR ,$extensionName);
        return $refactoredExtensionName;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 24.06.14
 * Time: 17:34
 */

namespace App\Services\Classes;

use App\Services\Interfaces\IDBManagerService;

class OracleDBManagerService extends AbstractSingletonService implements IDBManagerService
{

    /** @var  $connections - array like [0] - array('parameters' => $connectionParams , 'object' => $connection ) */
    protected $connections;

    /** returns connection if exists or creats it
     * @param string $server
     * @param string $user
     * @param string $password
     * @param string $database ;
     */
    public function getConnection($server, $user, $password, $database)
    {
        if (!$connection = $this->getConnectionByParams($server, $user, $database)) {
            $databaseType = 'oci8';
            /** @var \ADOConnection $db */
            $db = ADONewConnection($databaseType);
            $db->debug = true;
            $db->connectSID = true;
            $db->charSet = 'utf8';
            define('ADODB_OUTP',$this->getNewADODB_OUTP() );
            $db->Connect($server, $user, $password, $database);
            return $db;
        }
        return $connection;
    }

    /** @param string $server
     * @param string $user
     * @param string $database
     * @param bool $returnIndex - false if default
     */
    protected function getConnectionByParams($server, $user, $database, $returnIndex = false)
    {
        $parameters = array(
            'server' => $server,
            'user' => $user,
            'database' => $database);
        if (!empty($this->connections)) {
            foreach ($this->connections as $connectionIndex => $connectionRecord) {
                if ($connectionRecord['parameters'] == $parameters) {
                    return $returnIndex ? $connectionIndex : $connectionRecord['connection'];
                }
            }
        }
        return false;
    }

    /**
     * @param string $server
     * @param string $user
     * @param string $database
     * @param $connection
     * @param bool $replaceIfNotExists
     */
    protected function addConnectionByParams($server, $user, $database, $connection, $replaceIfNotExists = true)
    {
        if ($connectionIndex = $this->getConnectionByParams($server, $user, $database, true) && $replaceIfNotExists) {
            $this->connections[$connectionIndex] = array(
                'parameters' => array(
                    'server' => $server,
                    'user' => $user,
                    'database' => $database,
                ),
                'object' => $connection
            );
        } else if (!$replaceIfNotExists) {
            $this->connections[] = $connection;
        }
    }

    public static function getInstance()
    {
        ServiceManager::get('extension_manager')->getExtension('oracle_db');
        return parent::getInstance();
    }

    /** make select return query result
     * @param string $sql
     * @param \ADOConnection $connection
     */
    public function executeSql($sql, $connection)
    {
        return $connection->Execute($sql);

    }
    /** execute procedure using REF Cursor like $pr = "eas_user_login('asannikov', 'EF6489E73E70F8B927F649FD48F2D048', :refc)";
     * @param string $procedure
     * @param \ADOConnection $connection
     */
    public function executeCursor($procedure, $connection){
        $result = array();
        $fullPr = 'BEGIN ' . ' ' . $procedure . ';' . ' ' . 'END;';
        /** @var \ADORecordSet $recordSet */
        $recordSet = $connection->ExecuteCursor($fullPr, 'refc');
        while($arr = $recordSet->FetchRow()) {
            $result[] = $arr;
        }
        return $result;
    }

    /** change adodb output function */
    public function getNewADODB_OUTP() {
        requireLibrary('newADODB_OUTPFunc');
        return 'newADODB_OUTPFunc';
    }
}

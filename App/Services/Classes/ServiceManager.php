<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 10.06.14
 * Time: 15:46
 */

namespace App\Services\Classes;

use App\Services\Classes\DefaultConfigManager;
use App\Services\Interfaces\IConfigurationService;
use App\Services\Interfaces\IServiceManagerService;

/** use this class to get services, which are assigned for special work in /config.ini */
class ServiceManager extends AbstractSingletonService implements IServiceManagerService
{
    /** use get function to get some service assigned to specific work in config.ini
     * @param $serviceRoleName - name of specific work which service is assigned for in config.ini
     */
    public static function get($serviceRoleName)
    {
        //if there is no object of this class getInstance() will create it
        self::getInstance();
        /** @var IConfigurationService $configManager */
        $configManager = DefaultConfigManager::getInstance();
        $servicesAppointments = $configManager->get('services');
        $serviceRoleName = mb_strtolower($serviceRoleName);
        if (!empty($servicesAppointments[$serviceRoleName])) {
            $serviceClassName = $servicesAppointments[$serviceRoleName];
            return self :: doGet($serviceClassName);
        } else {
            throw new \Exception('no appointment on  ' . $serviceRoleName . ' role');
        }
    }

    /** doGet function get a service using its class name
     * @param string $serviceClassName
     */
    private static function doGet($serviceClassName)
    {
        $fullServiceClassName = __DIR__ . DIRECTORY_SEPARATOR . $serviceClassName;
        if (file_exists($fullServiceClassName . '.php')) {
            return call_user_func(array( __NAMESPACE__. '\\' .$serviceClassName, 'getInstance'));
        } else {
            throw new \Exception('can\'t find class ' . $fullServiceClassName);
        }
    }

}
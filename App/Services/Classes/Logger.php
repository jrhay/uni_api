<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 09.06.14
 * Time: 17:44
 */

namespace App\Services\Classes;


use App\Services\Interfaces\ILoggerService;

class Logger extends AbstractSingletonService implements ILoggerService
{
    /** log function takes message and logLevel as argument and logging it to file in /logs, you can use one of
     * standart log levels or yous our own.
     * @param string $message
     * @param string $log_level
     * @param array $dataArray
     * @param \DateTime $datetime
     * @return bool
     */
    public function log($message, $logLevel, $dataArray = null, $datetime = null)
    {
        if (is_null($datetime)) {
            $datetime = getNowTime();
        } else {
            $datetime = $datetime->format('Y-m-d H:i:s');
        }
        $logLevel = mb_strtolower($logLevel);
        $logFileName = $this->getLogFileName($logLevel);
        $message = "\n" . ' [ ' . $datetime . ' ] ' . $message;
        $dataString = '';
        if (!is_null($dataArray)) {
            $dataString = "\n" . var_export($dataArray, true);
        }
        $logResult = file_put_contents($logFileName, $message . $dataString, FILE_APPEND);
        return $logResult;
    }

    /** this function check log file size and, if it is too large, change name using number
     * @param string $logLevel
     * @return string
     */
    private function getLogFileName($logLevel)
    {
        $logFileName = $this->getLogDirectory() . DIRECTORY_SEPARATOR . $logLevel;
        $maxLogFileSize = ServiceManager::get('config_manager')->get('logs/max_file_size');
//        $fileSize = filesize($logFileName)/1024;
        if (file_exists($logFileName) and (filesize($logFileName)/1024) >= $maxLogFileSize*1024) {
            $time = getNowTime();
            $newName = $logFileName . " { last log - $time } ";
            rename($logFileName, $newName);
        }
        return $logFileName;
    }

    protected function getLogDirectory()
    {
        $logDir = ServiceManager::get('config_manager')->get('logs/log_directory');
        $fullLogDir = PROJECT_ROOT_DIR . DIRECTORY_SEPARATOR . $logDir;
        if (file_exists($fullLogDir)) {
            return $fullLogDir;
        } else {
            throw new \Exception('no log directory on the disk');
        }
    }
}
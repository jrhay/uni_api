<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 24.06.14
 * Time: 18:47
 */

namespace App\Services\Classes;


use App\Services\Interfaces\IExceptionHandlerService;
use App\Services\Interfaces\IExtensionManagerService;
use App\Services\Interfaces\ILoggerService;
use App\Services\Interfaces\IServiceManagerService;

ServiceManager::get('extension_manager')->requireExtensionFile('exception_handler');

class ExceptionThrowerWrapper extends \ExceptionThrower
{

    static function HandleError($code, $string, $file, $line, $context)
    {
        //Todo: debug ExceptionThrower it doesn't work
        /** @var ILoggerService $logger */
        $logger = ServiceManager::get('logger');
        $logger->log($string,'error');
        parent::HandleError($code, $string, $file, $line, $context);
    }
}
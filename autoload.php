<?php
spl_autoload_register(function ($class) {

    $classPath = str_replace('\\', DIRECTORY_SEPARATOR , $class );
    $file = __DIR__. DIRECTORY_SEPARATOR . $classPath . '.php';
//    echo ' require ' . $file;
    require_once __DIR__. DIRECTORY_SEPARATOR . $classPath . '.php';
});